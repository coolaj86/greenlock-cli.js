#!/bin/bash
#<pre><code>

# This is a 3 step process
#   1. First we need to figure out whether to use wget or curl for fetching remote files
#   2. Next we need to figure out whether to use unzip or tar for downloading releases
#   3. We need to actually install the stuff

set -e
set -u

###############################
#                             #
#         http_get            #
# boilerplate for curl / wget #
#                             #
###############################

# See https://git.coolaj86.com/coolaj86/snippets/blob/master/bash/http-get.sh

_my_http_get=""
_my_http_opts=""
_my_http_out=""

detect_http_get()
{
  set +e
  if type -p curl >/dev/null 2>&1; then
    _my_http_get="curl"
    _my_http_opts="-fsSL"
    _my_http_out="-o"
  elif type -p wget >/dev/null 2>&1; then
    _my_http_get="wget"
    _my_http_opts="--quiet"
    _my_http_out="-O"
  else
    echo "Aborted, could not find curl or wget"
    return 7
  fi
  set -e
}

http_get()
{
  $_my_http_get $_my_http_opts $_my_http_out "$2" "$1"
  touch "$2"
}

http_bash()
{
  _http_url=$1
  my_args=${2:-}
  rm -rf my-tmp-runner.sh
  $_my_http_get $_my_http_opts $_my_http_out my-tmp-runner.sh "$_http_url"; bash my-tmp-runner.sh $my_args; rm my-tmp-runner.sh
}

detect_http_get

###############################
##       END HTTP_GET        ##
###############################

echo ""
echo ""
echo ""

if [ -z "${GREENLOCK_PATH:-}" ]; then
  echo 'GREENLOCK_PATH="'${GREENLOCK_PATH:-}'"'
  GREENLOCK_PATH=/opt/greenlock
fi

echo "Installing Greenlock to '$GREENLOCK_PATH'"
echo ""

echo "sudo mkdir -p '$GREENLOCK_PATH'"
sudo mkdir -p "$GREENLOCK_PATH"
echo "sudo chown -R $(whoami) '$GREENLOCK_PATH'"
sudo chown -R $(whoami) "$GREENLOCK_PATH"

echo "Installing node.js dependencies into $GREENLOCK_PATH"
# until node v10.x gets fix for ursa we have no advantage to switching from 8.x
export NODEJS_VER=v8.11.1
export NODE_PATH="$GREENLOCK_PATH/lib/node_modules"
export NPM_CONFIG_PREFIX="$GREENLOCK_PATH"
export PATH="$GREENLOCK_PATH/bin:$PATH"
sleep 1
http_bash https://git.coolaj86.com/coolaj86/node-installer.sh/raw/branch/master/install.sh --no-dev-deps >/dev/null 2>/dev/null

my_tree="master"
my_node="$GREENLOCK_PATH/bin/node"
my_npm="$my_node $GREENLOCK_PATH/bin/npm"
my_tmp="$GREENLOCK_PATH/tmp"
mkdir -p $my_tmp

echo "Installing Greenlock into $GREENLOCK_PATH"
set +e
my_unzip=$(type -p unzip)
my_tar=$(type -p tar)
if [ -n "$my_unzip" ]; then
  rm -f $my_tmp/greenlock-$my_tree.zip
  http_get https://git.coolaj86.com/coolaj86/greenlock-cli.js/archive/$my_tree.zip $my_tmp/greenlock-$my_tree.zip
  # -o means overwrite, and there is no option to strip
  $my_unzip -o $my_tmp/greenlock-$my_tree.zip -d $GREENLOCK_PATH/ > /dev/null
  cp -ar  $GREENLOCK_PATH/greenlock-cli.js/* $GREENLOCK_PATH/ > /dev/null
  rm -rf $GREENLOCK_PATH/greenlock-cli.js
elif [ -n "$my_tar" ]; then
  rm -f $my_tmp/greenlock-$my_tree.tar.gz
  http_get https://git.coolaj86.com/coolaj86/greenlock-cli.js/archive/$my_tree.tar.gz $my_tmp/greenlock-$my_tree.tar.gz
  ls -lah $my_tmp/greenlock-$my_tree.tar.gz
  $my_tar -xzf $my_tmp/greenlock-$my_tree.tar.gz --strip 1 -C $GREENLOCK_PATH/
else
  echo "Neither tar nor unzip found. Abort."
  exit 13
fi
set -e

pushd $GREENLOCK_PATH >/dev/null
  $my_npm install >/dev/null 2>/dev/null
popd >/dev/null

cat << EOF > $GREENLOCK_PATH/bin/greenlock
#!/bin/bash
$my_node $GREENLOCK_PATH/bin/greenlock.js
EOF
chmod a+x $GREENLOCK_PATH/bin/greenlock
echo "Creating link to 'greenlock' in /usr/local/bin"
echo "sudo ln -sf $GREENLOCK_PATH/bin/greenlock /usr/local/bin/greenlock"
sudo ln -sf $GREENLOCK_PATH/bin/greenlock /usr/local/bin/greenlock

set +e
if type -p setcap >/dev/null 2>&1; then
  echo ""
  echo "Setting permissions to allow Greenlock to run on port 80 and port 443 without sudo or root"
  echo "sudo setcap cap_net_bind_service=+ep $GREENLOCK_PATH/bin/node"
  sudo setcap cap_net_bind_service=+ep $GREENLOCK_PATH/bin/node
fi
set -e

echo ""
echo ""
echo "Installed successfully. Try it out:"
echo ""
echo "  greenlock --help"
echo ""
echo ""

#sudo setcap cap_net_bind_service=+ep $GREENLOCK_PATH/bin/node

#https://git.coolaj86.com/coolaj86/greenlock-cli.js.git
#https://git.coolaj86.com/coolaj86/greenlock-cli.js/archive/:tree:.tar.gz
#https://git.coolaj86.com/coolaj86/greenlock-cli.js/archive/:tree:.zip
