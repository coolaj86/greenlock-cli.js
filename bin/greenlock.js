#!/usr/bin/env node
'use strict';

var cli = require('cli');
var mkdirp = require('mkdirp');

cli.parse({
  'acme-version':
    [ false, " ACME / Let's Encrypt version. v01 or draft-11 (aka v02)", 'string', 'draft-11' ]
, 'acme-url':
    [ false, " ACME Directory Resource URL", 'string', '' ]
, email:
    [ false, " Email used for registration and recovery contact. (default: null)", 'email' ]
, 'agree-tos': [ false, " Agree to the Let's Encrypt Subscriber Agreement", 'boolean', false ]
, 'community-member': [ false, " Submit stats to and get updates from Greenlock", 'boolean', false ]
, domains:
    [ false, " Domain names to apply. For multiple domains you can enter a comma separated list of domains as a parameter. (default: [])", 'string' ]
, 'renew-within': [ false, " Renew certificates this many days before expiry", 'int', 7 ]
, 'cert-path':
    [ false, " Path to where new cert.pem is saved", 'string'
    , ':configDir/live/:hostname/cert.pem' ]
, 'fullchain-path':
    [ false, " Path to where new fullchain.pem (cert + chain) is saved", 'string'
    , ':configDir/live/:hostname/fullchain.pem' ]
, 'bundle-path':
    [ false, " Path to where new bundle.pem (fullchain + privkey) is saved", 'string'
    , ':configDir/live/:hostname/bundle.pem' ]
, 'chain-path':
    [ false, " Path to where new chain.pem is saved", 'string'
    , ':configDir/live/:hostname/chain.pem' ]
, 'privkey-path':
    [ false, " Path to where privkey.pem is saved", 'string'
    , ':configDir/live/:hostname/privkey.pem' ]
, 'config-dir':
    [ false, " Configuration directory.", 'string'
    , '~/letsencrypt/etc/' ]
, 'http-01-port': [ false, " Use HTTP-01 challenge type with this port (only port 80 is valid with most production servers) (default: 80)", 'int' ]
, 'dns-01': [ false, " Use DNS-01 challange type", 'boolean', false ]
, standalone: [ false, " Obtain certs using a \"standalone\" webserver.", 'boolean', false ]
, manual: [ false, " Print the token and key to the screen and wait for you to hit enter, giving you time to copy it somewhere before continuing (default: false)", 'boolean', false ]
, debug: [ false, " show traces and logs", 'boolean', false ]
, 'root': [ false, " public_html / webroot path (may use the :hostname template such as /srv/www/:hostname)", 'string' ]

//
// backwards compat
//
, duplicate:
    [ false, " Allow getting a certificate that duplicates an existing one/is an early renewal", 'boolean', false ]
, 'rsa-key-size':
    [ false, " Size (in bits) of the RSA key.", 'int', 2048 ]
, server:
    [ false, " alias of acme-url for certbot compatibility", 'string', '' ]
, 'domain-key-path':
    [ false, " Path to privkey.pem to use for domain (default: generate new)", 'string' ]
, 'account-key-path':
    [ false, " Path to privkey.pem to use for account (default: generate new)", 'string' ]
, webroot: [ false, " for certbot compatibility", 'boolean', false ]
, 'webroot-path': [ false, "alias of '--root' for certbot compatibility", 'string' ]
//, 'standalone-supported-challenges': [ false, " Supported challenges, order preferences are randomly chosen. (default: http-01,tls-sni-01)", 'string', 'http-01,tls-sni-01']
, 'work-dir': [ false, "for certbot compatibility (ignored)", 'string', '~/letsencrypt/var/lib/' ]
, 'logs-dir': [ false, "for certbot compatibility (ignored)", 'string', '~/letsencrypt/var/log/' ]
});

// ignore certonly and extraneous arguments
cli.main(function(_, options) {
  console.log('');
  var args = {};
  var homedir = require('os').homedir();

  Object.keys(options).forEach(function (key) {
    var val = options[key];

    if ('string' === typeof val) {
      val = val.replace(/^~/, homedir);
    }

    key = key.replace(/\-([a-z0-9A-Z])/g, function (c) { return c[1].toUpperCase(); });
    args[key] = val;
  });

  Object.keys(args).forEach(function (key) {
    var val = args[key];

    if ('string' === typeof val) {
      val = val.replace(/(\:configDir)|(\:config)/, args.configDir);
    }

    args[key] = val;
  });

  if (args.domains) {
    args.domains = args.domains.split(',');
  }

  if (!(Array.isArray(args.domains) && args.domains.length) || !args.email || !args.agreeTos || !args.acmeVersion || (!args.server && !args.acmeUrl)) {
    console.error("\nUsage:\n\ngreenlock certonly --standalone \\");
    console.error("\t--acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \\");
    console.error("\t--agree-tos --email user@example.com --domains example.com \\");
    console.error("\t--config-dir ~/acme/etc \\");
    console.error("\nSee greenlock --help for more details\n");
    return;
  }

  if (args.http01Port) {
    // [@agnat]: Coerce to string. cli returns a number although we request a string.
    args.http01Port = "" + args.http01Port;
    args.http01Port = args.http01Port.split(',').map(function (port) {
      return parseInt(port, 10);
    });
  }

  mkdirp(args.configDir, function (err) {
    if (err) {
      console.error("Could not create --config-dir '" + args.configDir + "':", err.code);
      console.error("Try setting --config-dir '/tmp'");
      return;
    }

    require('../').run(args).then(function (status) {
      process.exit(status);
    });
  });
});
