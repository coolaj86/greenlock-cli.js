#!/bin/bash

## create a quick server if needed
# sudo mkdir -p /srv/www/example.com
# pushd /srv/www/example.com
#   sudo python -m SimpleHTTPServer 80 &
#   my_pid=$!

sudo greenlock certonly --webroot \
  --acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \
  --agree-tos --email jon@example.com --domains example.com \
  --community-member \
  --root /srv/www/example.com \
  --config-dir ~/acme/etc

#   kill $my_pid
# popd
