#!/bin/bash
sudo greenlock certonly --standalone \
  --acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \
  --agree-tos --email jon@example.com --domains example.com,www.example.com \
  --community-member \
  --config-dir ~/acme/etc
