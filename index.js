'use strict';

var DAY = 24 * 60 * 60 * 1000;

var Greenlock = require('greenlock');

module.exports.run = function (args) {
  var leChallenge;
  var leStore;
  var servers;
  var USE_DNS = {};
  var challengeType;

  args.acmeUrl = args.server = (args.acmeUrl || args.server);
  args.root = args.webrootPath = (args.root || args.webrootPath);
  if (args.dns01) {
    challengeType = 'dns-01';
    args.webrootPath = '';
    args.standalone = USE_DNS;
  } else /*if (args.http01Port)*/ {
    challengeType = 'http-01';
  }

  if (args.manual) {
    leChallenge = require('le-challenge-manual').create({});
  }
  else if (args.webrootPath) {
    // webrootPath is all that really matters here
    // TODO rename le-challenge-fs to le-challenge-webroot
    leChallenge = require('./lib/webroot').create({ webrootPath: args.webrootPath });
  }
  else if (USE_DNS !== args.standalone) {
    leChallenge = require('le-challenge-standalone').create({});
    servers = require('./lib/servers').create(leChallenge);
  }

  var privkeyPath = args.privkeyPath || args.domainKeyPath || ':configDir/live/:hostname/privkey.pem'; //args.privkeyPath
  leStore = require('le-store-certbot').create({
    configDir: args.configDir
  , privkeyPath: privkeyPath
  , fullchainPath: args.fullchainPath
  , certPath: args.certPath
  , chainPath: args.chainPath
  , bundlePath: args.bundlePath
  , webrootPath: args.root
  , domainKeyPath: args.domainKeyPath
  , accountKeyPath: args.accountKeyPath
  });

  if (!args.acmeUrl) {
    throw new Error("You must specify the ACME server url with --acme-url");
  }
  if (!args.acmeVersion) {
    throw new Error("You must specify the ACME API version with --acme-version");
  }

  // let Greenlock know that we're handling standalone / webroot here
  var leChallenges = {};
  leChallenges[challengeType] = leChallenge;
  var greenlock = Greenlock.create({
    debug: args.debug
  , server: args.acmeUrl
  , version: args.acmeVersion
  , store: leStore
  , challenges: leChallenges
  , renewWithin: args.renewWithin * DAY
  , duplicate: args.duplicate
  });

  if (servers) {
    if (args.tlsSni01Port) {
      servers.startServers(
        [], args.tlsSni01Port
      , { debug: args.debug, tlsOptions: greenlock.tlsOptions }
      );
    }
    else {
      servers.startServers(
        args.http01Port || [80], []
      , { debug: args.debug }
      );
    }
  }

  // Note: can't use args directly as null values will overwrite template values
  return greenlock.register({
    debug: args.debug
  , email: args.email
  , agreeTos: args.agreeTos
  , communityMember: args.communityMember
  , domains: args.domains
  , rsaKeySize: args.rsaKeySize
  , challengeType: challengeType
  }).then(function (certs) {
    if (!certs.renewing) {
      return certs;
    }
    console.log("");
    console.log("Got certificate(s) for " + certs.altnames.join(', '));
    console.log("\tIssued at " + new Date(certs.issuedAt).toISOString() + "");
    console.log("\tValid until " + new Date(certs.expiresAt).toISOString() + "");
    console.log("");
    console.log("Renewing them now");
    return certs.renewing;
  }).then(function (certs) {
    console.log("");
    console.log("Got certificate(s) for " + certs.altnames.join(', '));
    console.log("\tIssued at " + new Date(certs.issuedAt).toISOString() + "");
    console.log("\tValid until " + new Date(certs.expiresAt).toISOString() + "");
    console.log("");
    console.log('Private key installed at:');
    console.log(
      privkeyPath
      .replace(/:configDir/g, args.configDir)
      .replace(/:hostname/g, args.domains[0])
    );
    console.log("");

    // should get back account, path to certs, pems, etc?
    console.log('Certificates installed at:');
    console.log(
      [
      //  args.privkeyPath
        args.certPath
      , args.chainPath
      , args.fullchainPath
      , args.bundlePath || ''
      ].join('\n').replace(/\n+/g, '\n')
      .replace(/:configDir/g, args.configDir)
      .replace(/:hostname/g, args.domains[0])
    );
    console.log("");

    if (servers) {
      return servers.closeServers({ debug: args.debug }).then(function() {
        return 0;
      });
    }

    return 0;
  }, function (err) {
    console.error('[Error]: greenlock-cli');
    console.error(err.stack || new Error('get stack').stack);

    return 1;
  });

};
