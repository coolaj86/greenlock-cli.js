![Greenlock Logo](https://git.rootprojects.org/root/greenlock.js/raw/branch/master/logo/greenlock-1063x250.png "Greenlock Logo")

!["Greenlock Function"](https://git.rootprojects.org/root/greenlock.js/raw/branch/master/logo/from-not-secure-to-secure-url-bar.png "from url bar showing not secure to url bar showing secure")


# [Greenlock&trade;](https://git.rootprojects.org/root/greenlock-cli.js) for Web Servers | a [Root](https://rootprojects.org) project

Free SSL, Free Wildcard SSL, and Fully Automated HTTPS made dead simple<br>
<small>certificates issued by Let's Encrypt v2 via [ACME](https://git.rootprojects.org/root/acme-v2.js)</small>

!["Lifetime Downloads"](https://img.shields.io/npm/dt/greenlock.svg "Lifetime Download Count can't be shown")
!["Monthly Downloads"](https://img.shields.io/npm/dm/greenlock.svg "Monthly Download Count can't be shown")
!["Weekly Downloads"](https://img.shields.io/npm/dw/greenlock.svg "Weekly Download Count can't be shown")
!["Stackoverflow Questions"](https://img.shields.io/stackexchange/stackoverflow/t/greenlock.svg "S.O. Question count can't be shown")

| **Greenlock for Web Servers**
| [Greenlock for Web Browsers](https://git.rootprojects.org/root/greenlock.html)
| [Greenlock for Express.js](https://git.rootprojects.org/root/greenlock-express.js)
| [Greenlock&trade;.js](https://git.rootprojects.org/root/greenlock.js)
|

# Features

- [x] Commandline (cli) Certificate Manager (like certbot)
- [x] Integrated Web Server
- [x] Free SSL Certificates
- [x] Automatic certificate renewal before expiration
- [x] One-off standalone registration / renewal
- [x] On-the-fly registration / renewal via webroot

# Install

## Mac & Linux

Open Terminal and run this install script:

```bash
curl -fsS https://get.greenlock.app/ | bash
```

This will install greenlock to `/opt/greenlock` and put a symlink to
`/opt/greenlock/bin/greenlock` in `/usr/local/bin/greenlock` for convenience.

You can customize the installation:

```bash
export NODEJS_VER=v8.11.1
export GREENLOCK_PATH=/opt/greenlock
curl -fsS https://get.greenlock.app/ | bash
```

This will change which version of node.js is bundled with greenlock
and the path to which greenlock installs.

## Windows & Node.js

1. Install [node.js](https://nodejs.org)
2. Open _Node.js_
2. Run the command `npm install -g greenlock-cli`

# Usage

We have a few different examples of issuing SSL certificates:

* Standalone (testing): Issue a one-off certificate
* Webroot (production): Automatic certificate renewal for Apache, Nginx, HAProxy, etc
* Manual (debugging): Go through the certificate proccess step-by-step
<!-- * Server (production): Leave it all to Greenlock -->

**Important Note**: Staging vs Production

Each of these examples are using the **staging server**.

Once you've successfully gotten certificates with the staging server
you must **delete** `--config-dir` (i.e. `rm -rf ~/acme`) and then
switch to the **production server**.

```
--acme-version draft-11 --server https://acme-v02.api.letsencrypt.org/directory \
```

## Standalone

<small>**primarily for testing**</small>

You can run in standalone mode **on your server** and get a cert instantly.

Note: No other webserver may be running at the time (use Webroot mode for that).

```bash
sudo greenlock certonly --standalone \
  --acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \
  --agree-tos --email jon@example.com --domains example.com,www.example.com \
  --community-member \
  --config-dir ~/acme/etc
```

## WebRoot

<small>**for testing and production**</small>

With this method you must use **your existing http (port 80) server** (Apache, Nginx, HAProxy, etc).
You will specify the **path or template path** to your `public_html` or `www` webroot.

For example:

  * I want to get an SSL cert for `example.com`
  * `index.html` lives at `/srv/www/example.com`
  * I would use this command:

```bash
sudo greenlock certonly --webroot \
  --acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \
  --agree-tos --email jon@example.com --domains example.com \
  --community-member \
  --root /srv/www/example.com \
  --config-dir ~/acme/etc
```

Now let's say that

  * I have many sites in `/srv/www/`, all by their name
  * I already store my ssl certs in the format `/etc/apache/ssl/:hostname/{key.pem,ssl.crt}`
  * I'll run this command instead:

```bash
sudo greenlock certonly --webroot \
  --acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \
  --agree-tos --email jon@example.com --domains example.com,whatever.com,foobar.net \
  --community-member \
  --root "/srv/www/:hostname" \
  --privkey-path "/etc/apache/ssl/:hostname/key.pem" \
  --fullchain-path "/etc/apache/ssl/:hostname/ssl.crt" \
  --config-dir ~/acme/etc
```

### Run with cron

Those commands are safe to be run **daily** with cron.
The certificates will automatically renew 2 weeks before expiring.

## Interactive

<small>**primarily for debugging**</small>

The token (for all challenge types) and keyAuthorization (only for https-01)
will be printed to the screen and you will be given time to copy it wherever
(file, dns record, database, etc) and the process will complete once you hit `enter`.

```bash
sudo greenlock certonly --manual \
  --acme-version draft-11 --acme-url https://acme-staging-v02.api.letsencrypt.org/directory \
  --agree-tos --email jon@example.com --domains example.com \
  --community-member \
  --config-dir ~/acme/etc
```

# Certificate Locations
Then you can see your certs at `~/acme/etc/live`.

```
~/acme/etc/
└── example.com
    ├── cert.pem
    ├── chain.pem
    ├── fullchain.pem  (Apache, Nginx, node.js)
    ├── privkey.pem    (Apache, Nginx, node.js)
    └── bundle.pem     (HAProxy)
```


## Run without root (no sudo)

`sudo` is used to allow greenlock to use port 80 and write to httpd-owned directories.

Allow greenlock to bind on system ports without root:

```bash
sudo setcap cap_net_bind_service=+ep /opt/greenlock/bin/node
```

To allow greenlock to write to folders owned by another user, set it to run as that user.

Otherwise, you can change the permissions on the folders, which is
**probably a BAD IDEA**. Probabry a **security risk**.
But since some of you are going to do it anyway I might as well tell you how:

```
# PROBABLY A BAD IDEA
sudo chown -R $(whoami) /etc/ssl /etc/acme
```

# Command Line Options

```
Usage:
  greenlock [OPTIONS] [ARGS]

Options:
      --acme-version [STRING]   'draft-11' for Let's Encrypt v2 or 'v01' for Let's Encrypt v1. (default: null)

      --acme-url [URL]          Directory URL for ACME API. Let's Encrypt URLs are:
                                  draft-11
                                    https://acme-staging-v02.api.letsencrypt.org/directory
                                    https://acme-v02.api.letsencrypt.org/directory

                                  v01
                                    https://acme-staging.api.letsencrypt.org/directory
                                    https://acme-v01.api.letsencrypt.org/directory

      --email EMAIL             Email used for registration and recovery contact. (default: null)

      --agree-tos BOOLEAN       Agree to the Let's Encrypt Subscriber Agreement

      --community-member        Submit stats to and receive updates from Greenlock

      --domains HOSTNAME        Domain names to apply. For multiple domains you can enter a comma
                                separated list of domains as a parameter. (default: [])

      --renew-within [NUMBER]   Renew certificates this many days before expiry. (default: 10)

      --cert-path STRING        Path to where new cert.pem is saved
                                (Default is :conf/live/:hostname/cert.pem)

      --fullchain-path [STRING] Path to where new fullchain.pem (cert + chain) is saved
                                (Default is :conf/live/:hostname/fullchain.pem)

      --chain-path [STRING]     Path to where new chain.pem is saved
                                (Default is :conf/live/:hostname/chain.pem)

      --bundle-path [STRING]    Path to where new bundle.pem (fullchain + privkey) is saved
                                (Default is :conf/live/:hostname/bundle.pem)

      --domain-key-path STRING  Path to privkey.pem to use for domain (default: generate new)

      --account-key-path STRING Path to privkey.pem to use for account (default: generate new)

      --config-dir STRING       Configuration directory. (Default is ~/letsencrypt/etc/)

      --http-01-port [NUMBER]   Use HTTP-01 challenge type with this port, used for SimpleHttp challenge. (Default is 80)
                                (must be 80 with most production servers)

      --dns-01                  Use DNS-01 challenge type.

      --standalone [BOOLEAN]    Obtain certs using a "standalone" webserver.  (Default is true)

      --manual [BOOLEAN]        Print the token and key to the screen and wait for you to hit enter,
                                giving you time to copy it somewhere before continuing. (Default is false)

      --debug BOOLEAN           show traces and logs

  -h, --help                    Display help and usage details
```


# Certbot Command Line Options

These options are maintained for compatability with certbot:

```
      --server [STRING]         ACME Directory Resource URI. (Default is https://acme-v01.api.letsencrypt.org/directory))

      --duplicate BOOLEAN       Allow getting a certificate that duplicates an existing one/is
                                an early renewal.

      --webroot BOOLEAN         Obtain certs by placing files in a webroot directory.

      --webroot-path STRING     public_html / webroot path.
```

Note: some of the options may not be fully implemented. If you encounter a problem, please report a bug on the issues page.

# Legal &amp; Rules of the Road

Greenlock&trade; and Bluecrypt&trade; are [trademarks](https://rootprojects.org/legal/#trademark) of AJ ONeal

The rule of thumb is "attribute, but don't confuse". For example:

> Built with [Greenlock CLI](https://git.rootprojects.org/root/greenlock-cli.js) (a [Root](https://rootprojects.org) project).

Please [contact us](mailto:aj@therootcompany.com) if you have any questions in regards to our trademark,
attribution, and/or visible source policies. We want to build great software and a great community.

[Greenlock&trade;](https://git.rootprojects.org/root/greenlock.js) |
MPL-2.0 |
[Terms of Use](https://therootcompany.com/legal/#terms) |
[Privacy Policy](https://therootcompany.com/legal/#privacy)
